<?php
/**
 * Projects - Functions
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 */

require_once(DIR."modules/projects/classes/cProjectsScope.class.php");
require_once(DIR."modules/projects/classes/cProjectsActivity.class.php");
require_once(DIR."modules/projects/classes/cProjectsActivityStatus.class.php");
require_once(DIR."modules/projects/classes/cProjectsTask.class.php");
require_once(DIR."modules/projects/classes/cProjectsTaskStatus.class.php");
