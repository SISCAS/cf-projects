<?php
/**
 * Projects - Own Tasks
 *
 * @package Coordinator\Modules\Projects
 * @task Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-usage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("own_tasks"));
// get selected status
$selected_status_obj=new cProjectsTaskStatus($_REQUEST["status"]?:"processing");
// make status links array
$tasks_status_links_array=array();
// cycle all status
foreach(cProjectsTaskStatus::availables() as $status_fobj){
	// skip completed
	if($status_fobj->code=="completed"){continue;}
	// make url and label with task count
	$url=api_url(["scr"=>"own_tasks","status"=>$status_fobj->code]);
	$count=cProjectsTask::count(false,["fkUserReferent"=>$GLOBALS["session"]->user->id,"status"=>$status_fobj->code]);
	$label=api_label($count,null,"background-color:".$status_fobj->color)."&nbsp;&nbsp;".$status_fobj->text;
	// add status link to array
	$tasks_status_links_array[]=api_link($url,$label,null,"btn btn-sm btn-default".($status_fobj->code==$selected_status_obj->code?" active":null));
}
// switch selected status
switch($selected_status_obj->code){
	case "planned":
	case "processing":
		$status_date="planningWeek";
		break;
	case "testing":
		$status_date="executionEndDate";
		break;
	default:
		$status_date=null;
}
// get availables tasks
$tasks_array=cProjectsTask::availables(false,["fkUserReferent"=>$GLOBALS["session"]->user->id,"status"=>$selected_status_obj->code]);
// build table
$table=new strTable(api_text("own_tasks-tr-unvalued",strtolower($selected_status_obj->text)));
$table->addHeader("&nbsp;");
$table->addHeader(api_text("cProjectsTask-property-subject"),null,"100%");
if($status_date){$table->addHeader(api_text("cProjectsTask-property-".$status_date),"nowrap text-right");}
else{$table->addHeader(api_text("cProjectsActivity-property-deadline"),"nowrap text-right");}
$table->addHeader("&nbsp;");
// cycle all tasks
foreach(api_sortObjectsArray($tasks_array,($status_date?:"id")) as $task_fobj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_edit","idActivity"=>$task_fobj->getActivity()->id,"idTask"=>$task_fobj->id]),"fa-pencil",api_text("table-td-edit"),true,null,null,null,null,"_blank");
	$ob->addElement(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_status","idActivity"=>$task_fobj->getActivity()->id,"idTask"=>$task_fobj->id]),"fa-recycle",api_text("table-td-status"),true,null,null,null,null,"_blank");
	//if($task_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsTask","idTask"=>$task_fobj->id,"return"=>["scr"=>"tasks_list"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cProjectsTask-confirm-undelete"));}
	//else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsTask","idTask"=>$task_fobj->id,"return"=>["scr"=>"tasks_list"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cProjectsTask-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($task_fobj->id==$_REQUEST["idTask"]){$tr_class_array[]="currentrow";}
	if($task_fobj->deleted){$tr_class_array[]="deleted";}
	// make tasks row
	$table->addRow(implode(" ",$tr_class_array));
	$table->addRowFieldAction(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_view","idActivity"=>$task_fobj->getActivity()->id,"idTask"=>$task_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
	//$table->addRowField(api_tag("samp",$task_fobj->getActivity()->id.".".$task_fobj->id),"nowrap");
	$table->addRowField($task_fobj->getActivity()->subject." - ".$task_fobj->subject,"truncate-ellipsis");
	if($status_date=="planningWeek"){$table->addRowField(api_week_format($task_fobj->planningWeek,"W-Y"),"nowrap text-right");}
	elseif($status_date=="executionEndDate"){$table->addRowField(api_date_format($task_fobj->executionEndDate,api_text("date")),"nowrap text-right");}
	else{$table->addRowField(api_date_format($task_fobj->getActivity()->deadline,api_text("date")),"nowrap text-right");}
	// add operation button to table
	$table->addRowField($ob->render(),"nowrap text-right");
}
// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol(api_tag("p",implode(" ",$tasks_status_links_array)),"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_status_obj){api_dump($selected_status_obj,"selected status");}
