--
-- CRM - Update (2.0.10)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `projects__activities`
--

ALTER TABLE `projects__activities` DROP `status`;

--
-- Alter table `projects__tasks`
--

ALTER TABLE `projects__tasks` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
