--
-- CRM - Update (2.0.9)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `projects__tasks`
--

ALTER TABLE `projects__tasks` ADD `ticket` VARCHAR(16) NULL AFTER `description`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
