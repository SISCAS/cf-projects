--
-- Projects - Setup (2.0.0)
--
-- @package Coordinator\Modules\Projects
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `projects__scopes`
--

CREATE TABLE IF NOT EXISTS `projects__scopes` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkArea` int(11) unsigned NOT NULL,
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `fkArea` (`fkArea`),
	CONSTRAINT `projects__scopes_ibfk_1` FOREIGN KEY (`fkArea`) REFERENCES `organization__areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects__scopes__logs`
--

CREATE TABLE IF NOT EXISTS `projects__scopes__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `projects__scopes__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `projects__scopes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `projects__scopes__logs`:
--   `fkObject`
--       `projects__scopes` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects__activities`
--

CREATE TABLE IF NOT EXISTS `projects__activities` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`status` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`fkScope` int(11) unsigned NOT NULL,
	`fkArea` int(11) unsigned NOT NULL,
	`fkUserReferent` int(11) unsigned NOT NULL,
	`subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`description` text COLLATE utf8_unicode_ci NOT NULL,
	`applicant` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
	`deadline` date DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `fkProject` (`fkScope`),
	KEY `fkArea` (`fkArea`),
	CONSTRAINT `projects__activities_ibfk_1` FOREIGN KEY (`fkScope`) REFERENCES `projects__scopes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `projects__activities_ibfk_2` FOREIGN KEY (`fkArea`) REFERENCES `organization__areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects__activities__logs`
--

CREATE TABLE IF NOT EXISTS `projects__activities__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `projects__activities__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `projects__activities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects__tasks`
--

CREATE TABLE IF NOT EXISTS `projects__tasks` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`status` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`fkActivity` int(11) unsigned NOT NULL,
	`fkDepartment` int(11) unsigned NOT NULL,
	`fkUserReferent` int(11) unsigned NOT NULL,
	`subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	`planningDays` int(11) DEFAULT NULL,
	`planningWeek` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
	`executionStartDate` date DEFAULT NULL,
	`executionEndDate` date DEFAULT NULL,
	`completionDate` date DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `fkActivity` (`fkActivity`),
	KEY `fkDepartment` (`fkDepartment`),
	CONSTRAINT `projects__tasks_ibfk_1` FOREIGN KEY (`fkActivity`) REFERENCES `projects__activities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `projects__tasks_ibfk_2` FOREIGN KEY (`fkDepartment`) REFERENCES `organization__departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects__tasks__logs`
--

CREATE TABLE IF NOT EXISTS `projects__tasks__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `projects__tasks__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `projects__tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('projects-manage','projects',1),
('projects-usage','projects',2);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
