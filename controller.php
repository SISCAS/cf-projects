<?php
/**
 * Projects - Controller
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 */

// debug
api_dump($_REQUEST,"_REQUEST");
// check if object controller function exists
if(function_exists($_REQUEST["obj"]."_controller")){
	// call object controller function
	call_user_func($_REQUEST["obj"]."_controller",$_REQUEST["act"]);
}else{
	api_alerts_add(api_text("alert_controllerObjectNotFound",[MODULE,$_REQUEST["obj"]."_controller"]),"danger");
	api_redirect("?mod=".MODULE);
}

/**
 * Scope controller
 *
 * @param string $action Object action
 */
function cProjectsScope_controller($action){
	// check authorizations
	api_checkAuthorization("projects-manage","dashboard");
	// get object
	$scope_obj=new cProjectsScope($_REQUEST["idScope"]);
	api_dump($scope_obj,"Scope object");
	// check object
	if($action!="store" && !$scope_obj->exists()){api_alerts_add(api_text("cProjectsScope-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"scopes"]));}
	// execution
	try{
		switch($action){
			case "store":
				$scope_obj->store($_REQUEST);
				api_alerts_add(api_text("cProjectsScope-alert-stored"),"success");
				break;
			case "delete":
				$scope_obj->delete();
				api_alerts_add(api_text("cProjectsScope-alert-deleted"),"warning");
				break;
			case "undelete":
				$scope_obj->undelete();
				api_alerts_add(api_text("cProjectsScope-alert-undeleted"),"warning");
				break;
			case "remove":
				$scope_obj->remove();
				api_alerts_add(api_text("cProjectsScope-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Scope action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"management","tab"=>"scopes","idScope"=>$scope_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"scopes","idScope"=>$scope_obj->id]),"cProjectsScope-alert-error");
	}
}

/**
 * Activity controller
 *
 * @param string $action Object action
 */
function cProjectsActivity_controller($action){
	// check authorizations
	api_checkAuthorization("projects-usage","dashboard");
	// get object
	$activity_obj=new cProjectsActivity($_REQUEST["idActivity"]);
	api_dump($activity_obj,"activity object");

	/** @todo check authorization */

	// check object
	if($action!="store" && !$activity_obj->exists()){api_alerts_add(api_text("cProjectsActivity-alert-exists"),"danger");api_redirect(api_url(["scr"=>"activities_list"]));}
	// execution
	try{
		switch($action){
			case "store":
				$activity_obj->store($_REQUEST);
				api_alerts_add(api_text("cProjectsActivity-alert-stored"),"success");
				// check for notification
				if(!$_REQUEST['idActivity'] && $GLOBALS['session']->user->id!=$activity_obj->fkUserReferent){
					$url=URL."index.php".api_url(["scr"=>"activities_view","idActivity"=>$activity_obj->id]);
					api_mail_save(api_text("cProjectsActivity-notify-created-subject",$activity_obj->id),api_text("cProjectsActivity-notify-created-message",[$activity_obj->id,$activity_obj->subject,nl2br($activity_obj->description),$url]),$activity_obj->getReferent()->mail);
				}
				break;
			case "status":
				$activity_obj->status($_REQUEST["status"]);
				api_alerts_add(api_text("cProjectsActivity-alert-status"),"success");
				break;
			case "archive":
				$activity_obj->store(["archived"=>true]);
				api_alerts_add(api_text("cProjectsActivity-alert-archived"),"success");
				break;
			case "unarchive":
				$activity_obj->store(["archived"=>false]);
				api_alerts_add(api_text("cProjectsActivity-alert-unarchived"),"success");
				break;
			case "delete":
				$activity_obj->delete();
				api_alerts_add(api_text("cProjectsActivity-alert-deleted"),"warning");
				break;
			case "undelete":
				$activity_obj->undelete();
				api_alerts_add(api_text("cProjectsActivity-alert-undeleted"),"warning");
				break;
			case "remove":
				$activity_obj->remove();
				api_alerts_add(api_text("cProjectsActivity-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Activity action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"activities_list","idActivity"=>$activity_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"activities_list","idActivity"=>$activity_obj->id]),"cProjectsActivity-alert-error");
	}
}

/**
 * Task controller
 *
 * @param string $action Object action
 */
function cProjectsTask_controller($action){
	// check authorizations
	api_checkAuthorization("projects-usage","dashboard");
	// get object
	$task_obj=new cProjectsTask($_REQUEST["idTask"]);
	api_dump($task_obj,"task object");

	/** @todo check authorization */

	// check object
	if($action!="store" && !$task_obj->exists()){api_alerts_add(api_text("cProjectsTask-alert-exists"),"danger");api_redirect(api_url(["scr"=>"activities_list"]));}
	// execution
	try{
		switch($action){
			case "store":
				$task_obj->store($_REQUEST);
				api_alerts_add(api_text("cProjectsTask-alert-stored"),"success");
				// check for notification
				if(!$_REQUEST['idTask'] && $GLOBALS['session']->user->id!=$task_obj->fkUserReferent){
					$url=URL."index.php".api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_view","idActivity"=>$task_obj->getActivity()->id,"idTask"=>$task_obj->id]);
					api_mail_save(api_text("cProjectsTask-notify-created-subject",$task_obj->getActivity()->id."-".$task_obj->id),api_text("cProjectsTask-notify-created-message",[$task_obj->getActivity()->id."-".$task_obj->id,$task_obj->getActivity()->subject,$task_obj->subject,nl2br($task_obj->description),$url]),$task_obj->getReferent()->mail);
				}
				break;
			case "status":
				$task_obj->status($_REQUEST["status"]);
				api_alerts_add(api_text("cProjectsTask-alert-status"),"success");
				break;
			case "delete":
				$task_obj->delete();
				api_alerts_add(api_text("cProjectsTask-alert-deleted"),"warning");
				break;
			case "undelete":
				$task_obj->undelete();
				api_alerts_add(api_text("cProjectsTask-alert-undeleted"),"warning");
				break;
			case "remove":
				$task_obj->remove();
				api_alerts_add(api_text("cProjectsTask-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Task action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"activities_list","idTask"=>$task_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"activities_list","idTask"=>$task_obj->id]),"cProjectsTask-alert-error");
	}
}
