<?php
/**
 * Projects - Activities View (Informations)
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cProjectsActivity $activity_obj
 */

// build informations description list
$informations_dl=new strDescriptionList("br","dl-horizontal");
$informations_dl->addElement(api_text("cProjectsActivity-property-description"),nl2br($activity_obj->description));
if($activity_obj->progress){$informations_dl->addElement(api_text("cProjectsActivity-property-progress"),nl2br($activity_obj->progress));}
