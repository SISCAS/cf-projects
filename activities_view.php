<?php
/**
 * Projects - Activities View
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-usage","dashboard");
// get objects
$activity_obj=new cProjectsActivity($_REQUEST["idActivity"]);
// check objects
if(!$activity_obj->exists()){api_alerts_add(api_text("cProjectsActivity-alert-exists"),"danger");api_redirect(api_url(["scr"=>"activities_list"]));}
/** @todo check authorization */
//if(!$activity_obj->viewable()){api_alerts_add(api_text("cProjectsActivity-alert-denied"),"warning");api_redirect(api_url(["scr"=>"activities_list","idActivity"=>$activity_obj->id]));}
// deleted alert
if($activity_obj->deleted){api_alerts_add(api_text("cProjectsActivity-warning-deleted"),"warning");}
// include module activity
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("activities_view",$activity_obj->subject));
// build description list
$left_dl=new strDescriptionList("br","dl-horizontal");
$left_dl->addElement(api_text("cObject-property-status"),$activity_obj->getStatus()->getLabel(true,true,"left"));
$left_dl->addElement(api_text("cProjectsActivity-property-subject"),api_tag("strong",$activity_obj->subject));
$left_dl->addElement(api_text("cProjectsActivity-property-fkScope"),$activity_obj->getScope()->getLabelPopup());
$left_dl->addElement(api_text("cProjectsActivity-property-fkUserReferent"),$activity_obj->getReferent()->fullname);
// build right description list
$right_dl=new strDescriptionList("br","dl-horizontal");
$right_dl->addElement(api_text("cProjectsActivity-property-fkArea"),$activity_obj->getArea()->getLabelPopup());
$right_dl->addElement(api_text("cProjectsActivity-property-applicant"),$activity_obj->applicant);    /** @todo api per deadline colorata */
if($activity_obj->deadline){$right_dl->addElement(api_text("cProjectsActivity-property-deadline"),api_date_format($activity_obj->deadline,api_text("date")));}
// check for tab
if(!defined(TAB)){define("TAB","informations");}
/**
 * Informations
 *
 * @var strDescriptionList $informations_dl
 */
require_once(MODULE_PATH."activities_view-informations.inc.php");
/**
 * Tasks
 *
 * @var strTable $tasks_table
 * @var cProjectsActivityTask $selected_task_obj
 */
require_once(MODULE_PATH."activities_view-tasks.inc.php");
// build tabs
$tab=new strTab();
$tab->addItem(api_icon("fa-flag-o")." ".api_text("activities_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
$tab->addItem(api_icon("fa-list")." ".api_text("activities_view-tab-tasks"),$tasks_table->render(),("tasks"==TAB?"active":null));
$tab->addItem(api_icon("fa-file-text-o")." ".api_text("activities_view-tab-logs"),api_logs_table($activity_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render(),("logs"==TAB?"active":null));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($left_dl->render(),"col-xs-12 col-md-7");
$grid->addCol($right_dl->render(),"col-xs-12 col-md-5");
$grid->addRow();
$grid->addCol($tab->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_task_obj){api_dump($selected_task_obj,"selected task");}
api_dump($activity_obj,"activity");
