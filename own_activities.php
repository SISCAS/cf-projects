<?php
/**
 * Projects - Own Activities
 *
 * @package Coordinator\Modules\Projects
 * @activity Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-usage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("own_activities"));
// get selected status
$selected_status=($_REQUEST["status"]?:"active");
// count activities
$count_active=0;
$count_inactive=0;
$count_completed=0;
// get availables activities
$activities_array=cProjectsActivity::availables(false,["fkUserReferent"=>$GLOBALS["session"]->user->id]);
// build table
$table=new strTable(api_text("own_activities-tr-unvalued"));
$table->addHeader("&nbsp;");
$table->addHeader("#","text-center");
$table->addHeader(api_text("cProjectsActivity-property-subject"),null,"100%");
$table->addHeader(api_text("cProjectsActivity-property-deadline"),"nowrap text-right");
$table->addHeader("&nbsp;");
// cycle all activities
foreach(api_sortObjectsArray($activities_array,"subject") as $activity_fobj){
	// count activities based on status
	${"count_".$activity_fobj->getStatus()->code}++;
	// skip activities not in current selected status
	if($activity_fobj->getStatus()->code!=$selected_status){continue;}
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"activities_edit","idActivity"=>$activity_fobj->id]),"fa-pencil",api_text("table-td-edit"),true,null,null,null,null,"_blank");
	$ob->addElement(api_url(["scr"=>"activities_view","act"=>"activity_status","idActivity"=>$activity_fobj->id]),"fa-recycle",api_text("table-td-status"),true,null,null,null,null,"_blank");
	//if($activity_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsActivity","idActivity"=>$activity_fobj->id,"return"=>["scr"=>"own_activities"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cProjectsActivity-confirm-undelete"));}
	//else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsActivity","idActivity"=>$activity_fobj->id,"return"=>["scr"=>"own_activities"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cProjectsActivity-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($activity_fobj->id==$_REQUEST["idActivity"]){$tr_class_array[]="currentrow";}
	if($activity_fobj->deleted){$tr_class_array[]="deleted";}
	// make activities row
	$table->addRow(implode(" ",$tr_class_array));
	$table->addRowFieldAction(api_url(["scr"=>"activities_view","act"=>"activity_view","idActivity"=>$activity_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
	$table->addRowField(api_tag("samp",$activity_fobj->id),"nowrap");
	$table->addRowField($activity_fobj->subject,"truncate-ellipsis");
	$table->addRowField(api_date_format($activity_fobj->deadline,api_text("date")),"nowrap text-right");
	// add operation button to table
	$table->addRowField($ob->render(),"nowrap text-right");
}
// make status links array
$activities_status_new_links_array=array();
$activities_status_new_links_array[]=api_link(api_url(["scr"=>"own_activities","status"=>"inactive"]),api_label($count_inactive,null,"background-color:#999999")."&nbsp;&nbsp;".api_text("own_activities-status-inactive"),null,"btn btn-sm btn-default".("inactive"==$selected_status?" active":null));
$activities_status_new_links_array[]=api_link(api_url(["scr"=>"own_activities","status"=>"active"]),api_label($count_active,null,"background-color:#039BE5")."&nbsp;&nbsp;".api_text("own_activities-status-active"),null,"btn btn-sm btn-default".("active"==$selected_status?" active":null));
$activities_status_new_links_array[]=api_link(api_url(["scr"=>"own_activities","status"=>"completed"]),api_label($count_completed,null,"background-color:#43A047")."&nbsp;&nbsp;".api_text("own_activities-status-completed"),null,"btn btn-sm btn-default".("completed"==$selected_status?" active":null));
// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol(api_tag("p",implode(" ",$activities_status_new_links_array)),"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
