<?php
/**
 * Projects - Activity
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 */

// build application
$app=new strApplication();
// build nav
$nav=new strNav("nav-tabs");
// dashboard
$nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
// management
if(in_array(SCRIPT,array("dashboard","management"))){if(api_checkAuthorization("projects-manage")){$nav->addItem(api_text("nav-management"),api_url(["scr"=>"management"]));}}

/**
 * Activities
 *
 * @var cProjectsActivity $activity_obj
 */
if(api_script_prefix()=="activities"){
	$nav->addItem(api_text("nav-activities-list"),api_url(["scr"=>"activities_list"]));
	if(SCRIPT=="activities_edit" && !$activity_obj->exists()){$nav->addItem(api_text("nav-activities-add"),api_url(["scr"=>"activities_edit"]));}
	elseif(is_object($activity_obj) && $activity_obj->exists() && in_array(SCRIPT,array("activities_view","activities_edit"))){
		$nav->addItem(api_text("nav-operations"),null,null,"active");
		$nav->addSubItem(api_text("nav-activities-operations-edit"),api_url(["scr"=>"activities_edit","idActivity"=>$activity_obj->id]));
		if($activity_obj->archived){$nav->addSubItem(api_text("nav-activities-unarchive"),api_url(["scr"=>"controller","act"=>"unarchive","obj"=>"cProjectsActivity","idActivity"=>$activity_obj->id,"return"=>api_return(["scr"=>"activities_view"])]));}
		else{$nav->addSubItem(api_text("nav-activities-archive"),api_url(["scr"=>"controller","act"=>"archive","obj"=>"cProjectsActivity","idActivity"=>$activity_obj->id,"return"=>api_return(["scr"=>"activities_view"])]));}
		$nav->addSubItem(api_text("nav-activities-operations-task_add"),api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_add","idActivity"=>$activity_obj->id]));
	}
	$nav->addItem(api_text("activities_gantt"),api_url(["scr"=>"activities_gantt"]));
}

/**
 * Own
 */
if(api_script_prefix()=="own"){
	$nav->addItem(api_text("own_gantt"),api_url(["scr"=>"own_gantt"]));
	$nav->addItem(api_text("own_activities"),api_url(["scr"=>"own_activities"]));
	$nav->addItem(api_text("own_tasks"),api_url(["scr"=>"own_tasks"]));
}

// add nav to html
$app->addContent($nav->render(false));
