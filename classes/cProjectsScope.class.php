<?php
/**
 * Projects - Scope
 *
 * @package Coordinator\Modules\Projects
 * @area Cogne Acciai Speciali s.p.a
 */

/**
 * Projects, Scope class
 */
class cProjectsScope extends cObject{

	/** Parameters */
	static protected $table="projects__scopes";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $fkArea;
	protected $name;
	protected $description;

	/**
	 * Check
	 *
	 * {@inheritdoc}
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->fkArea))){throw new Exception("Scope, area key is mandatory..");}
		if(!strlen(trim($this->name))){throw new Exception("Scope, name is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Label
	 *
	 * @return string Scope label
	 */
	public function getLabel(){
		$label=$this->name;
		if($this->description){$label.=" (".$this->description.")";}
		return $label;
	}

	/**
	 * Get label with name and description in popup
	 *
	 * @return string
	 */
	public function getLabelPopup(){return api_link("#",$this->name,$this->description,"hidden-link",true);}

	/**
	 * Get Area
	 *
	 * @return cOrganizationArea
	 */
	public function getArea(){return new cOrganizationArea($this->fkArea);}

	/**
	 * Get Activities
	 *
	 * @param boolean $deleted Get also deleted entries
	 * @return object[]|false Array of entries objects or false
	 */
	public function getActivities($deleted=false){return cProjectsActivity::availables($deleted,["fkScope"=>$this->id]);}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// get and sorts areas
		$areas_array=array();
		foreach(cOrganizationArea::availables($this->exists()) as $area_fobj){$areas_array[$area_fobj->id]=$area_fobj->getLabel();}
		asort($areas_array);
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"projects","scr"=>"controller","act"=>"store","obj"=>"cProjectsScope","idScope"=>$this->id],$additional_parameters)),"POST",null,null,"projects__scope-edit_form");
		// fields
		$form->addField("select","fkArea",api_text("cProjectsScope-property-fkArea"),$this->fkArea,api_text("cProjectsScope-placeholder-fkArea"),null,null,null,"required");
		foreach($areas_array as $area_fkey=>$area_flabel){$form->addFieldOption($area_fkey,$area_flabel);}
		$form->addField("text","name",api_text("cProjectsScope-property-name"),$this->name,api_text("cProjectsScope-placeholder-name"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cProjectsScope-property-description"),$this->description,api_text("cProjectsScope-placeholder-description"),null,null,null,"rows='2'");
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Remove
	 *
	 * {@inheritdoc}
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Scope remove function disabled by developer.. (enabled in debug mode)");}
		if(count($this->getActivities(true))){throw new Exception("Scope remove function disabled for already used scopes");}
		else{parent::remove();}
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
