<?php
/**
 * Projects - Task Status
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Projects, Task Status class
 */
class cProjectsTaskStatus extends cTranscoding{

	/** {@inheritdoc} */
	protected static function datas(){
		return array(
		 ["suspended",api_text("cProjectsTaskStatus-suspended"),"fa-hand-stop-o","#999999"],
		 ["inserted",api_text("cProjectsTaskStatus-inserted"),"fa-edit","#FB8C00"],
		 ["analyzed",api_text("cProjectsTaskStatus-analyzed"),"fa-commenting-o","#FFB300"],
		 ["planned",api_text("cProjectsTaskStatus-planned"),"fa-calendar-o","#039BE5"],
		 ["processing",api_text("cProjectsTaskStatus-processing"),"fa-spinner","#00897B"],
		 ["testing",api_text("cProjectsTaskStatus-testing"),"fa-comments-o","#43A047"],
		 ["completed",api_text("cProjectsTaskStatus-completed"),"fa-check-square-o","#757575"]
		);
	}

}
