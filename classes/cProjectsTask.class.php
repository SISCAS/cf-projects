<?php
/**
 * Projects - Task
 *
 * @package Coordinator\Modules\Projects
 * @activity Cogne Acciai Speciali s.p.a
 */

/**
 * Projects, Task class
 */
class cProjectsTask extends cObject{

	/** Parameters */
	static protected $table="projects__tasks";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	//protected $order;
	protected $status;
	protected $fkActivity;
	protected $fkDepartment;
	protected $fkUserReferent;
	protected $subject;
	protected $description;
	protected $progress;
	protected $ticket;
	protected $planningDays;
	protected $planningWeek;
	protected $executionStartDate;
	protected $executionEndDate;
	protected $completionDate;

	/**
	 * Check
	 *
	 * {@inheritdoc}
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->status))){$this->status="inserted";}
		if(!strlen(trim($this->fkActivity))){throw new Exception("Task, activity key is mandatory..");}
		if(!strlen(trim($this->fkDepartment))){throw new Exception("Task, department key is mandatory..");}
		if(!strlen(trim($this->fkUserReferent))){throw new Exception("Task, referent user key is mandatory..");}
		if(!strlen(trim($this->subject))){throw new Exception("Task, subject is mandatory..");}
		// return
		return true;
	}

	/**
	 * Decode log properties
	 *
	 * {@inheritdoc}
	 */
	public static function log_decode($event,$properties){
		// make return array
		$return_array=array();
		// check for status event
		if($event=="status"){$return_array[]=(new cProjectsTaskStatus($properties["status"]["previous"]))->getLabel(true,false)." &rarr; ".(new cProjectsTaskStatus($properties["status"]["current"]))->getLabel(true,false);}
		// return
		return implode(" | ",$return_array);
	}

	/**
	 * Get Status
	 *
	 * @return cProjectsTaskStatus
	 */
	public function getStatus(){return new cProjectsTaskStatus($this->status);}

	/**
	 * Get Activity
	 *
	 * @return cProjectsActivity
	 */
	public function getActivity(){return new cProjectsActivity($this->fkActivity);}

	/**
	 * Get Department
	 *
	 * @return cOrganizationDepartment
	 */
	public function getDepartment(){return new cOrganizationDepartment($this->fkDepartment);}

	/**
	 * Get Referent
	 *
	 * @return cUser
	 */
	public function getReferent(){return new cUser($this->fkUserReferent);}

	/**
	 * GANTT Week Duration
	 *
	 * @return integer|false Number of week for task duration
	 */
	public function gantt_weekDuration(){
		if(!$this->planningDays){return false;}
		return ceil($this->planningDays/5);
	}

	/**
	 * GANTT Week Start
	 *
	 * @return integer|false Starting week number (in numeric format)
	 */
	public function gantt_weekStart(){
		if(!$this->planningWeek){return false;}
		$week=$this->planningWeek;
		return api_week_numeric($week);
	}

	/**
	 * GANTT Week End
	 *
	 * @return string|false Ending week number (in numeric format)
	 */
	public function gantt_weekEnd(){
		if(!$this->planningWeek || !$this->planningDays){return false;}
		$endWeek=$this->gantt_weekStart();
		for($i=1;$i<$this->gantt_weekDuration();$i++){
			$endWeek++;
			if(!api_week_check($endWeek)){
				$endWeek=(substr($endWeek,0,4)+1)."01";
			}
		}
		return $endWeek;
	}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"projects","scr"=>"controller","act"=>"store","obj"=>"cProjectsTask","idTask"=>$this->id],$additional_parameters)),"POST",null,null,"projects__task-edit_form");
		// fields
		$form->addField("select","fkActivity",api_text("cProjectsTask-property-fkActivity"),$this->fkActivity,api_text("cProjectsTask-placeholder-fkActivity"),null,null,null,"required");
		foreach(cProjectsActivity::availables($this->exists()) as $activity_fobj){$form->addFieldOption($activity_fobj->id,$activity_fobj->subject);}  /** @todo check authorization */
		$form->addField("select","fkDepartment",api_text("cProjectsTask-property-fkDepartment"),$this->fkDepartment,api_text("cProjectsTask-placeholder-fkDepartment"),null,null,null,"required");
		foreach(cOrganizationDepartment::availables($this->exists()) as $department_fobj){$form->addFieldOption($department_fobj->id,$department_fobj->getLabel());}
		$form->addField("select","fkUserReferent",api_text("cProjectsTask-property-fkUserReferent"),$this->fkUserReferent,api_text("cProjectsTask-placeholder-fkUserReferent"),null,null,null,"required");

		/** @todo sostituire poi con nuovo cobject user */
		$users_array=array();
		$results=$GLOBALS['database']->queryObjects("SELECT * FROM `framework__users` ORDER BY lastname,firstname");
		foreach($results as $result){$form->addFieldOption($result->id,$result->lastname." ".$result->firstname);}

		$form->addField("text","subject",api_text("cProjectsTask-property-subject"),$this->subject,api_text("cProjectsTask-placeholder-subject"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cProjectsTask-property-description"),$this->description,api_text("cProjectsTask-placeholder-description"),null,null,null,"rows='3'");
		$form->addField("text","ticket",api_text("cProjectsTask-property-ticket"),$this->ticket,api_text("cProjectsTask-placeholder-ticket"));
		$form->addField("number","planningDays",api_text("cProjectsTask-property-planningDays"),$this->planningDays,api_text("cProjectsTask-placeholder-planningDays"));
		$form->addField("week","planningWeek",api_text("cProjectsTask-property-planningWeek"),$this->planningWeek);
		$form->addField("textarea","progress",api_text("cProjectsTask-property-progress"),$this->progress,api_text("cProjectsTask-placeholder-progress"),null,null,null,"rows='3'");
		$form->addField("date","executionStartDate",api_text("cProjectsTask-property-executionStartDate"),$this->executionStartDate);
		$form->addField("date","executionEndDate",api_text("cProjectsTask-property-executionEndDate"),$this->executionEndDate);
		$form->addField("date","completionDate",api_text("cProjectsTask-property-completionDate"),$this->completionDate);
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Plan form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_plan(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"projects","scr"=>"controller","act"=>"store","obj"=>"cProjectsTask","idTask"=>$this->id],$additional_parameters)),"POST",null,null,"projects__task-plan_form");
		// fields
		$form->addField("static","subject",api_text("cProjectsTask-property-subject"),api_tag("strong",$this->subject));
		$form->addField("week","planningWeek",api_text("cProjectsTask-property-planningWeek"),$this->planningWeek);
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Status form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_status(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"projects","scr"=>"controller","act"=>"status","obj"=>"cProjectsTask","idTask"=>$this->id],$additional_parameters)),"POST",null,null,"projects__task-status_form");
		// fields
		$form->addField("static","subject",api_text("cProjectsTask-property-subject"),api_tag("strong",$this->subject));
		$form->addField("radio","status",api_text("cObject-property-status"),$this->status,null,null,null,null,"required");
		foreach(cProjectsTaskStatus::availables() as $status_fobj){$form->addFieldOption($status_fobj->code,$status_fobj->getLabel(true,true),null,null,null,($status_fobj->code!=$this->status));}
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Status
	 *
	 * {@inheritdoc}
	 */
	public function status($status,array $additional_properties=null,$log=true){
		// check parameters
		if(!array_key_exists($status,cProjectsTaskStatus::availables())){throw new Exception("Task, status ".$status." was not defined..");}
		// switch status to update relative date
		switch($status){
			case "processing":if(!$this->executionStartDate){$additional_properties["executionStartDate"]=api_date();}break;
			case "testing":if(!$this->executionEndDate){$additional_properties["executionEndDate"]=api_date();}break;
			case "completed":if(!$this->completionDate){$additional_properties["completionDate"]=api_date();}break;
		}
		// reset completition date if not completed
		if($status!="completed" && $this->completionDate){$additional_properties["completionDate"]=null;}
		// call parent
		return parent::status($status,$additional_properties,$log);
	}

	/**
	 * Remove
	 *
	 * {@inheritdoc}
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Task remove function disabled by developer.. (enabled in debug mode)");}
		else{parent::remove();}
	}

	/**
	 * Event Triggered
	 *
	 * {@inheritdoc}
	 */
	/*protected function event_triggered($event){
		//api_dump($event,static::class." event triggered");
		// skip trace events
		if($event->typology=="trace"){return;}
		// log event to subsidiary
		$this->getActivity()->event_log($event->typology,$event->action,array_merge(["_obj"=>"cProjectsTask","_id"=>$this->id],$event->properties));
	}*/

}
