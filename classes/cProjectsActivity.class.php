<?php
/**
 * Projects - Activity
 *
 * @package Coordinator\Modules\Projects
 * @scope Cogne Acciai Speciali s.p.a
 */

/**
 * Projects, Activity class
 */
class cProjectsActivity extends cObject{

	/** Parameters */
	static protected $table="projects__activities";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $archived;
	protected $fkScope;
	protected $fkArea;
	protected $fkUserReferent;
	protected $subject;
	protected $description;
	protected $progress;
	protected $applicant;
	protected $deadline;

	/**
	 * Check
	 *
	 * {@inheritdoc}
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->fkScope))){throw new Exception("Activity, scope key is mandatory..");}
		if(!strlen(trim($this->fkArea))){throw new Exception("Activity, area key is mandatory..");}
		if(!strlen(trim($this->fkUserReferent))){throw new Exception("Activity, referent user key is mandatory..");}
		if(!strlen(trim($this->subject))){throw new Exception("Activity, subject is mandatory..");}
		if(!strlen(trim($this->description))){throw new Exception("Activity, description is mandatory..");}
		if(!strlen(trim($this->applicant))){throw new Exception("Activity, applicant is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Status
	 *
	 * @return cProjectsActivityStatus
	 */
	public function getStatus(){
		// check archived
		if($this->archived){
			$status="archived";
		}else{
			// get tasks
			$tasks_array=$this->getTasks(false);
			// set as inactive
			$status="inactive";
			// count completed
			$completed_counter=0;
			// cycle all tasks
			foreach($tasks_array as $task_fobj){
				// if at least one task is active set as active and return
				if(in_array($task_fobj->status,array("planned","processing","testing"))){
					$status="active";
					break;
				}
				// check for completed task
				if($task_fobj->status=="completed"){$completed_counter++;}
			}
			// check if all tasks is completed
			if($completed_counter>0 && $completed_counter==count($tasks_array)){$status="completed";}
		}
		// return
		return new cProjectsActivityStatus($status);
	}

	/**
	 * Get Scope
	 *
	 * @return cProjectsScope
	 */
	public function getScope(){return new cProjectsScope($this->fkScope);}

	/**
	 * Get Area
	 *
	 * @return cOrganizationArea
	 */
	public function getArea(){return new cOrganizationArea($this->fkArea);}

	/**
	 * Get Referent
	 *
	 * @return cUser
	 */
	public function getReferent(){return new cUser($this->fkUserReferent);}

	/**
	 * Get Tasks
	 *
	 * @param boolean $deleted Get also deleted entries
	 * @return object[]|false Array of entries objects or false
	 */
	public function getTasks($deleted=true){return cProjectsTask::availables($deleted,["fkActivity"=>$this->id]);}

	/**
	 * GANTT Planning Days
	 *
	 * @return int Total planning days of tasks
	 */
	public function gantt_planningDays(){
		$days=0;
		foreach($this->getTasks(false) as $task_fobj){$days+=$task_fobj->planningDays;}
		return $days;
	}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"projects","scr"=>"controller","act"=>"store","obj"=>"cProjectsActivity","idActivity"=>$this->id],$additional_parameters)),"POST",null,null,"projects__activity-edit_form");
		// fields
		$form->addField("select","fkScope",api_text("cProjectsActivity-property-fkScope"),$this->fkScope,api_text("cProjectsActivity-placeholder-fkScope"),null,null,null,"required");
		foreach(cProjectsScope::availables($this->exists()) as $scope_fobj){$form->addFieldOption($scope_fobj->id,$scope_fobj->getArea()->name." - ".$scope_fobj->getLabel());}  /** @todo check authorization */
		$form->addField("select","fkArea",api_text("cProjectsActivity-property-fkArea"),$this->fkArea,api_text("cProjectsActivity-placeholder-fkArea"),null,null,null,"required");
		foreach(cOrganizationArea::availables($this->exists()) as $area_fobj){$form->addFieldOption($area_fobj->id,$area_fobj->getLabel());}
		$form->addField("select","fkUserReferent",api_text("cProjectsActivity-property-fkUserReferent"),$this->fkUserReferent,api_text("cProjectsActivity-placeholder-fkUserReferent"),null,null,null,"required");

		/** @todo sostituire poi con nuovo cobject user */
		$users_array=array();
		$results=$GLOBALS['database']->queryObjects("SELECT * FROM `framework__users` ORDER BY lastname,firstname");
		foreach($results as $result){$form->addFieldOption($result->id,$result->lastname." ".$result->firstname);}

		$form->addField("text","subject",api_text("cProjectsActivity-property-subject"),$this->subject,api_text("cProjectsActivity-placeholder-subject"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cProjectsActivity-property-description"),$this->description,api_text("cProjectsActivity-placeholder-description"),null,null,null,"rows='9' required");
		$form->addField("text","applicant",api_text("cProjectsActivity-property-applicant"),$this->applicant,api_text("cProjectsActivity-placeholder-applicant"),null,null,null,"required");
		$form->addField("date","deadline",api_text("cProjectsActivity-property-deadline"),$this->deadline,api_text("cProjectsActivity-placeholder-deadline"));

		$form->addField("textarea","progress",api_text("cProjectsActivity-property-progress"),$this->progress,api_text("cProjectsActivity-placeholder-progress"),null,null,null,"rows='3'");

		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Remove
	 *
	 * {@inheritdoc}
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Activity remove function disabled by developer.. (enabled in debug mode)");}
		if(count($this->getTasks(true))){throw new Exception("Activity remove function disabled for already used activities");}
		else{parent::remove();}
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
