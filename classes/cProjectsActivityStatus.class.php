<?php
/**
 * Projects - Activity Status
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Projects, Activity Status class
 */
class cProjectsActivityStatus extends cTranscoding{

	/** {@inheritdoc} */
	protected static function datas(){
		return array(
		 ["active",api_text("cProjectsActivityStatus-active"),"fa-toggle-on","#039BE5"],
		 ["inactive",api_text("cProjectsActivityStatus-inactive"),"fa-toggle-off","#999999"],
		 ["completed",api_text("cProjectsActivityStatus-completed"),"fa-check-square-o","#43A047"],
		 ["archived",api_text("cProjectsActivityStatus-archived"),"fa-archive","#43A047"]
		);
	}

}
