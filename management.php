<?php
/**
 * Projects - Management
 *
 * @package Coordinator\Modules\Projects
 * @scope Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-manage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("management"));
// check for tab
if(!defined(TAB)){define("TAB","scopes");}
// build navigation
$nav=new strNav("nav-pills");
$nav->addItem(api_text("management-nav-scopes"),api_url(["scr"=>"management","tab"=>"scopes"]));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($nav->render(false),"col-xs-12");
// switch tab
switch(TAB){
	/**
	 * Scopes
	 *
	 * @var strTable $scopes_table
	 * @var cProjectsArea $selected_scope_obj
	 */
	case "scopes":
		// include tab
		require_once(MODULE_PATH."management-scopes.inc.php");
		$grid->addRow();
		$grid->addCol($scopes_table->render(),"col-xs-12");
		break;
}
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_scope_obj){api_dump($selected_scope_obj,"selected scope");}
