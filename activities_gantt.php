<?php
/**
 * Projects - Activities GANTT
 *
 * @package Coordinator\Modules\Projects
 * @task Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-usage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("activities_gantt"));
// build filter
$filter=new strFilter();
$filter->addItem(api_text("cProjectsScope"),api_transcodingsFromObjects(cProjectsScope::availables()),"fkScope");
$filter->addItem(api_text("cOrganizationArea"),api_transcodingsFromObjects(cOrganizationArea::availables()),"fkArea");
/** @todo sostituire poi con nuovo cobject user */
$users_array=array();
$results=$GLOBALS['database']->queryObjects("SELECT * FROM `framework__users` ORDER BY lastname,firstname");
foreach($results as $result){$users_array[$result->id]=$result->lastname." ".$result->firstname;}
$filter->addItem(api_text("cProjectsActivity-property-fkUserReferent"),$users_array,"fkUserReferent");
// get conditions
$conditions_array=$filter->getActiveFiltersConditions();
// activities and tasks definitions
$activities_array=array();
$activities_tasks_array=array();
$activities_gantt_weeks_array=array();
// get activities
$activities_array=cProjectsActivity::availables(false,$conditions_array);
// cycle all activities and get tasks
foreach($activities_array as $activity_fobj){
	// skip completed activities
	if($activity_fobj->getStatus()->code=="completed"){continue;}
	// get activities tasks
	$tasks_array=$activity_fobj->getTasks(false);
	// remove suspended, inserted and completed tasks
	foreach($tasks_array as $key=>$task_fobj){if(in_array($task_fobj->status,array("suspended","inserted","completed"))){unset($tasks_array[$key]);}}
	// chekc for tasks and add to activities tasks array
	if(count($tasks_array)){$activities_tasks_array[$activity_fobj->id]=$tasks_array;}
}
// gantt definitions
$gantt_weeks_array=array();
$gantt_week_start=date("YW");
$gantt_week_end=date("YW");
// get gantt week minimum
$gantt_week_minimum=$_GET["minimum"];
if(!$gantt_week_minimum){$gantt_week_minimum=date("YW")-3;}
// cycle all activities
foreach($activities_tasks_array as $activity_id=>$tasks_array){
	// reset activity sort
	$activity_gantt_week=new stdClass();
	$activity_gantt_week->start=999999;
	$activity_gantt_week->end=000000;
	// cycle all tasks
	foreach($tasks_array as $task_id=>$task_fobj){
		// get task weeks
		$week_start=$task_fobj->gantt_weekStart();
		$week_end=$task_fobj->gantt_weekEnd();
		// check and replace weeks
		if($week_start>0 && $week_start<$gantt_week_start){$gantt_week_start=$week_start;}
		if($week_end>$gantt_week_end){$gantt_week_end=$week_end;}
		if($week_start>0 && $week_start<$activity_gantt_week->start){$activity_gantt_week->start=$week_start;}
		if($week_end>$activity_gantt_week->end){$activity_gantt_week->end=$week_end;}
	}
	$activities_gantt_weeks_array[$activity_id]=$activity_gantt_week;
}
// sort activities by start week
api_sortObjectsArray($activities_gantt_weeks_array,"start");
//api_dump($gantt_week_start." -> ".$gantt_week_end,"gantt weeks");
// cycle all weeks
for($w=$gantt_week_start;$w<=$gantt_week_end;$w++){
	// skip not real weeks
	if(!api_week_check($w)){continue;}
	// skip oldest weeks
	if($w<$gantt_week_minimum){continue;}
	// add week to array
	$gantt_weeks_array[$w]=substr($w,-2);
}
//api_dump($gantt_weeks_array,"gantt weeks array");
// build table
$table=new strTable(api_text("activities_gantt-tr-unvalued"));
$table->fixColumns(7);
$table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
$table->addHeader(api_text("cProjectsActivity-property-subject")." / ".api_text("cProjectsTask-property-subject"),"nowrap",null,"min-width:270px");
$table->addHeader(api_text("cProjectsActivity-property-fkUserReferent"),"nowrap text-right",null,"min-width:90px");
$table->addHeader("&nbsp;");
$table->addHeader(api_text("cProjectsTask-property-planningDays"),"nowrap text-right");
$table->addHeader("&nbsp;");
$table->addHeader("&nbsp;");
foreach($gantt_weeks_array as $value=>$label){$table->addHeader($label,"text-center",16,($value==date("YW")?"background-color:#D9EDF7":null));}
$table->addHeader("&nbsp;");
// cycle all activities
foreach($activities_gantt_weeks_array as $activity_id=>$activity_gantt_week){
	// get activity
	$activity_fobj=$activities_array[$activity_id];
	// make activity row
	$table->addRow();
	$table->addRowFieldAction(api_url(["scr"=>"activities_view","tab"=>"informations","idActivity"=>$activity_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
	$table->addRowField($activity_fobj->getArea()->getLabelPopup()." - ".$activity_fobj->subject,"truncate-ellipsis");
	$table->addRowField($activity_fobj->getReferent()->fullname,"nowrap text-right");
	$table->addRowField($activity_fobj->getStatus()->getLabel(false,true),"nowrap text-right");
	$table->addRowField($activity_fobj->gantt_planningDays(),"nowrap text-center");
	$table->addRowField(api_date_format($activity_fobj->deadline,api_text("date")),"nowrap text-right");
	$table->addRowFieldAction("#null","fa-calendar",api_text("table-td-toggle"),null,null,null,"onClick='toggle(".$activity_fobj->id.")'");
	// cycle all gant weeks
	foreach($gantt_weeks_array as $value=>$label){
		if($value>=$activity_gantt_week->start && $value<=$activity_gantt_week->end){$td=api_label("&nbsp;","label-info");}else{$td="&nbsp;";}
		// check for start and build link
		if($value==$gantt_week_minimum && $activity_gantt_week->start && $activity_gantt_week->start<$gantt_week_minimum){$td=api_link(api_url(["scr"=>"activities_gantt","act"=>"","minimum"=>$activity_gantt_week->start]),api_label("<",($activity_gantt_week->end<$gantt_week_minimum?"label-default":"label-info")));}
		$table->addRowField($td,"text-center",($value==date("YW")?"background-color:#D9EDF7":null));
	}
	// cycle all activity tasks
	foreach(api_sortObjectsArray($activities_tasks_array[$activity_fobj->id],"planningWeek",false) as $task_fobj){
		// make table row class
		$tr_class_array=array("task","parent_".$activity_fobj->id);
		if($task_fobj->id==$_REQUEST["idTask"]){$tr_class_array[]="currentrow";}
		if($task_fobj->deleted){$tr_class_array[]="deleted";}
		// make tasks row
		$table->addRow(implode(" ",$tr_class_array));
		$table->addRowFieldAction(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_view","idActivity"=>$task_fobj->getActivity()->id,"idTask"=>$task_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
		$table->addRowField("&bull; ".$task_fobj->subject,"truncate-ellipsis");
		$table->addRowField($task_fobj->getReferent()->fullname,"nowrap text-right");
		$table->addRowField($task_fobj->getStatus()->getLabel(false,true),"nowrap text-right");
		$table->addRowField($task_fobj->planningDays,"nowrap text-center");
		$table->addRowField(api_week_format($task_fobj->planningWeek,"W-Y"),"nowrap text-right");
		$table->addRowFieldAction(api_url(["scr"=>"activities_gantt","act"=>"task_plan","idTask"=>$task_fobj->id]),"fa-calendar-plus-o",api_text("activities_gantt-td-plan"));
		// cycle all gant weeks
		foreach($gantt_weeks_array as $value=>$label){
			if($value>=$task_fobj->gantt_weekStart() && $value<=$task_fobj->gantt_weekEnd()){$td=api_label("&nbsp;","label-primary");}else{$td="&nbsp;";}
			// check for start and build link
			if($value==$gantt_week_minimum && $task_fobj->gantt_weekStart() && $task_fobj->gantt_weekStart()<$gantt_week_minimum){$td=api_link(api_url(["scr"=>"activities_gantt","act"=>"","minimum"=>$task_fobj->gantt_weekStart()]),api_label("<",($task_fobj->gantt_weekEnd()<$gantt_week_minimum?"label-default":"label-primary")));}
			$table->addRowField($td,"text-center",($value==date("YW")?"background-color:#D9EDF7":null));
		}
	}
}
// check for plan action
if(ACTION=="task_plan" && $_REQUEST['idTask']){
	// get object
	$selected_task_obj=new cProjectsTask($_REQUEST['idTask']);
	// get form
	$form=$selected_task_obj->form_plan(["return"=>api_return(["scr"=>"activities_gantt"])]);
	// add status
	$form->addField("hidden","status",null,(in_array($selected_task_obj->status,array("inserted","analyzed"))?"planned":$selected_task_obj->status));
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	// build modal
	$modal=new strModal(api_text("activities_gantt-tasks-modal-title"),null,"activities_gantt-tasks-move");
	$modal->setBody($form->render(2));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_activities_gantt-tasks-move').modal({show:true,backdrop:'static',keyboard:false});});");
}
// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol($filter->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// tasks hidden scripts
$app->addScript("$(document).ready(function(){\$('.task').addClass('hidden');$(document).trigger('stickyTable');});");
$app->addScript("function toggle(parent_id){\$('.parent_'+parent_id).toggleClass('hidden');$(document).trigger('stickyTable');}");
// renderize application
$app->render();
