<?php
/**
 * Projects - Activities View (Tasks)
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cProjectsActivity $activity_obj
 */

// build tasks table
$tasks_table=new strTable(api_text("activities_view-tasks-tr-unvalued"));
$tasks_table->addHeader("&nbsp;");
$tasks_table->addHeader(api_text("cProjectsTask-property-subject"),null,"100%");
$tasks_table->addHeader(api_text("cProjectsTask-property-fkUserReferent"),"nowrap text-right");
$tasks_table->addHeader(api_text("cProjectsTask-property-planningWeek"),"nowrap text-right");
$tasks_table->addHeader("&nbsp;");

// cycle all tasks
foreach(api_sortObjectsArray($activity_obj->getTasks(),"planningWeek") as $task_fobj){

	/** @todo check authorizations */

	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_view","idActivity"=>$activity_obj->id,"idTask"=>$task_fobj->id]),"fa-info-circle",api_text("table-td-view"));
	$ob->addElement(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_status","idActivity"=>$activity_obj->id,"idTask"=>$task_fobj->id]),"fa-recycle",api_text("table-td-status"));  //,(api_checkAuthorization("projects_manage"))
	$ob->addElement(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_edit","obj"=>"cProjectsTask","idActivity"=>$activity_obj->id,"idTask"=>$task_fobj->id]),"fa-pencil",api_text("table-td-edit"));
	if($task_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsTask","idActivity"=>$activity_obj->id,"idTask"=>$task_fobj->id,"return"=>["scr"=>"activities_view","tab"=>"tasks","idActivity"=>$activity_obj->id]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cProjectsTask-confirm-undelete"));}
	else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsTask","idActivity"=>$activity_obj->id,"idTask"=>$task_fobj->id,"return"=>["scr"=>"activities_view","tab"=>"tasks","idActivity"=>$activity_obj->id]]),"fa-trash",api_text("table-td-delete"),true,api_text("cProjectsTask-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($task_fobj->id==$_REQUEST["idTask"]){$tr_class_array[]="currentrow";}
	if($task_fobj->deleted){$tr_class_array[]="deleted";}
	// make row
	$tasks_table->addRow(implode(" ",$tr_class_array));
	$tasks_table->addRowField($task_fobj->getStatus()->getLabel(false,true),"nowrap");
	$tasks_table->addRowField($task_fobj->subject);
	$tasks_table->addRowField($task_fobj->getReferent()->fullname,"nowrap text-right");
	$tasks_table->addRowField($task_fobj->planningWeek,"nowrap text-right");
	$tasks_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="task_view" && $_REQUEST['idTask']){
	// get selected scope
	$selected_task_obj=new cProjectsTask($_REQUEST["idTask"]);
	// build description list
	$task_left_dl=new strDescriptionList("br","dl-horizontal");
	$task_left_dl->addElement(api_text("cObject-property-status"),$selected_task_obj->getStatus()->getLabel(true,true));
	$task_left_dl->addElement(api_text("cProjectsTask-property-subject"),api_tag("strong",$selected_task_obj->subject));
	$task_left_dl->addElement(api_text("cProjectsTask-property-fkUserReferent"),$selected_task_obj->getReferent()->fullname);
	$task_left_dl->addElement(api_text("cProjectsTask-property-fkDepartment"),$selected_task_obj->getDepartment()->getLabelPopup());
	if($selected_task_obj->ticket){$task_left_dl->addElement(api_text("cProjectsTask-property-ticket"),$selected_task_obj->ticket);}
	// build description list
	$task_right_dl=new strDescriptionList("br","dl-horizontal");
	if($selected_task_obj->planningDays){$task_right_dl->addElement(api_text("cProjectsTask-property-planningDays"),$selected_task_obj->planningDays);}
	if($selected_task_obj->planningWeek){$task_right_dl->addElement(api_text("cProjectsTask-property-planningWeek"),$selected_task_obj->planningWeek);}
	if($selected_task_obj->executionStartDate){$task_right_dl->addElement(api_text("cProjectsTask-property-executionStartDate"),api_date_format($selected_task_obj->executionStartDate,api_text("date")));}
	if($selected_task_obj->executionEndDate){$task_right_dl->addElement(api_text("cProjectsTask-property-executionEndDate"),api_date_format($selected_task_obj->executionEndDate,api_text("date")));}
	if($selected_task_obj->completionDate){$task_right_dl->addElement(api_text("cProjectsTask-property-completionDate"),api_date_format($selected_task_obj->completionDate,api_text("date")));}
	// build description list
	$task_bottom_dl=new strDescriptionList("br","dl-horizontal");
	if($selected_task_obj->description){$task_bottom_dl->addElement(api_text("cProjectsTask-property-description"),nl2br($selected_task_obj->description));}
	if($selected_task_obj->progress){$task_bottom_dl->addElement(api_text("cProjectsTask-property-progress"),nl2br($selected_task_obj->progress));}
	// actions bar
	$task_actions_bar=api_link(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_status","idActivity"=>$activity_obj->id,"idTask"=>$selected_task_obj->id]),api_text("activities_view-tasks-modal-btn-status"),null,"btn btn-default");
	$task_actions_bar.=api_link(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_edit","obj"=>"cProjectsTask","idActivity"=>$activity_obj->id,"idTask"=>$selected_task_obj->id]),api_text("activities_view-tasks-modal-btn-edit"),null,"btn btn-default");
	// build grid
	$grid=new strGrid();
	$grid->addRow();
	$grid->addCol($task_left_dl->render(),"col-xs-12 col-md-7");
	$grid->addCol($task_right_dl->render(),"col-xs-12 col-md-5");
	$grid->addRow();
	$grid->addCol($task_bottom_dl->render(),"col-xs-12");
	// build modal
	$modal=new strModal(api_text("activities_view-tasks-modal-title-view",$activity_obj->id),null,"activities_view-tasks-view");
	$modal->setSize("large");
	$modal->setBody($grid->render(false)."<hr>".api_logs_table($selected_task_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
	$modal->setFooter($task_actions_bar);
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_activities_view-tasks-view').modal();});");
}

// check for status action
if(ACTION=="task_status" && $_REQUEST['idTask']){ // && api_checkAuthorization("projects-manage")
	// get object
	$selected_task_obj=new cProjectsTask($_REQUEST['idTask']);
	// get form
	$form=$selected_task_obj->form_status(["return"=>api_return(["scr"=>"activities_view","tab"=>"tasks","idActivity"=>$activity_obj->id])]);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	// build modal
	$modal=new strModal(api_text("activities_view-tasks-modal-title-status",$activity_obj->id),null,"activities_view-tasks-status");
	$modal->setBody($form->render(2));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_activities_view-tasks-status').modal({show:true,backdrop:'static',keyboard:false});});");
	$app->addScript("$(function(){\$('#form_projects__task-status_form_control_submit').prop('disabled',true);});");
	$app->addScript("$(function(){\$('input[name=status]').change(function(){\$('#form_projects__task-status_form_control_submit').prop('disabled',false);});});");
	if($selected_task_obj->ticket){$app->addScript("$(function(){\$('input[name=status]').change(function(){if(['testing','completed'].includes(\$('input[name=status]:checked').val())){alert(\"".api_text("activities_view-tasks-modal-alert-ticket",$selected_task_obj->ticket)."\");}});});");}
}

// check for actions
if(in_array(ACTION,["task_add","task_edit"])){  // && api_checkAuthorization("projects-usage")

	/** @todo check authorizations */

	// get selected task
	$selected_task_obj=new cProjectsTask($_REQUEST["idTask"]);
	// get form
	$form=$selected_task_obj->form_edit(["return"=>api_return(["scr"=>"activities_view","tab"=>"tasks","idActivity"=>$activity_obj->id])]);
	// replace fkActivity
	$form->removeField("fkActivity");
	$form->addField("hidden","fkActivity",null,$activity_obj->id);
	// check if exists for date fields
	if(!$selected_task_obj->exists()){
		$form->removeField("planningWeek");
		$form->removeField("executionStartDate");
		$form->removeField("executionEndDate");
		$form->removeField("completionDate");
	}
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_task_obj->exists()){
		if($selected_task_obj->isDeleted()){
			$form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsTask","idActivity"=>$activity_obj->id,"idTask"=>$selected_task_obj->id,"return"=>["scr"=>"activities_view","tab"=>"tasks","idActivity"=>$activity_obj->id]]),"btn-warning",api_text("cProjectsTask-confirm-undelete"));
			$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cProjectsTask","idActivity"=>$activity_obj->id,"idTask"=>$selected_task_obj->id,"return"=>["scr"=>"activities_view","tab"=>"tasks","idActivity"=>$activity_obj->id]]),"btn-danger",api_text("cProjectsTask-confirm-remove"));
		}else{$form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsTask","idActivity"=>$activity_obj->id,"idTask"=>$selected_task_obj->id,"return"=>["scr"=>"activities_view","tab"=>"tasks","idActivity"=>$activity_obj->id]]),"btn-danger",api_text("cProjectsTask-confirm-delete"));}
	}
	// build modal
	$modal=new strModal(api_text("activities_view-tasks-modal-title-".($selected_task_obj->exists()?"edit":"add"),$activity_obj->id),null,"activities_view-tasks-edit");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_activities_view-tasks-edit').modal({show:true,backdrop:'static',keyboard:false});});");
	$app->addScript("$(document).ready(function(){\$('select[name=\"fkDepartment\"]').select2({allowClear:true,placeholder:\"".api_text("cProjectsTask-placeholder-fkDepartment")."\",dropdownParent:\$('#modal_activities_view-tasks-edit')});});");
	$app->addScript("$(document).ready(function(){\$('select[name=\"fkUserReferent\"]').select2({allowClear:true,placeholder:\"".api_text("cProjectsTask-placeholder-fkUserReferent")."\",dropdownParent:\$('#modal_activities_view-tasks-edit')});});");
}
