<?php
/**
 * Projects
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// definitions
$module_name="projects";
$module_repository_url="https://bitbucket.org/SISCAS/cf-projects/";
$module_repository_version_url="https://bitbucket.org/SISCAS/cf-projects/raw/master/VERSION.txt";
$module_required_modules=array("organization");
