<?php
/**
 * Projects - Activities Edit
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-usage","dashboard");
// get objects
$activity_obj=new cProjectsActivity($_REQUEST["idActivity"]);

// check authorizations

// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(($activity_obj->exists()?api_text("activities_edit",$activity_obj->subject):api_text("activities_edit-new")));
//$app->setTitle(api_text("activities_edit",($activity_obj->exists()?$activity_obj->subject:api_text("add"))));
// get form
$form=$activity_obj->form_edit(["return"=>api_return(["scr"=>"activities_view"])]);
// additional controls
if($activity_obj->exists()){
	$form->addControl("button",api_text("form-fc-cancel"),api_return_url(["scr"=>"activities_view","idActivity"=>$activity_obj->id]));
	if(!$activity_obj->deleted){
		$form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsActivity","idActivity"=>$activity_obj->id]),"btn-danger",api_text("cProjectsActivity-confirm-delete"));
	}else{
		$form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsActivity","idActivity"=>$activity_obj->id,"return"=>["scr"=>"activities_view"]]),"btn-warning");
		$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cProjectsActivity","idActivity"=>$activity_obj->id]),"btn-danger",api_text("cProjectsActivity-confirm-remove"));
	}
}else{$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"activities_list"]));}
// scripts
$app->addScript("$(document).ready(function(){\$('select[name=\"fkUserReferent\"]').select2({allowClear:true,placeholder:\"".api_text("cProjectsActivity-placeholder-fkUserReferent")."\"});});");
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($form->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($activity_obj,"activity");
