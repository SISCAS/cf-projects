<?php
/**
 * Projects - Own GANTT
 *
 * @package Coordinator\Modules\Projects
 * @task Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-usage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("own_gantt"));
// activities and tasks definitions
$activities_array=array();
$activities_tasks_array=array();
// get tasks
$tasks_array=cProjectsTask::availables(false,["fkUserReferent"=>$GLOBALS["session"]->user->id,"status"=>["analyzed","planned","processing","testing"]]);
// cycle all tasks sorted by planning week
foreach(array_reverse(api_sortObjectsArray($tasks_array,"planningWeek"),true) as $task_fobj){
	// check and get activity
	if(!array_key_exists($task_fobj->fkActivity,$activities_array)){$activities_array[$task_fobj->fkActivity]=new cProjectsActivity($task_fobj->fkActivity);}
	// add task to activy tasks
	$activities_tasks_array[$task_fobj->fkActivity][$task_fobj->id]=$task_fobj;
}
// gantt definitions
$gantt_weeks_array=array();
$gantt_week_start=date("YW");
$gantt_week_end=date("YW");
// get gantt week minimum
$gantt_week_minimum=$_GET["minimum"];
if(!$gantt_week_minimum){$gantt_week_minimum=date("YW")-3;}
// cycle all tasks
foreach($tasks_array as $task_fobj){
	// get task weeks
	$week_start=$task_fobj->gantt_weekStart();
	$week_end=$task_fobj->gantt_weekEnd();
	// check and replace weeks
	if($week_start>0&&$week_start<$gantt_week_start){$gantt_week_start=$week_start;}
	if($week_end>$gantt_week_end){$gantt_week_end=$week_end;}
}
// drop tasks array
unset($tasks_array);
// cycle all weeks
for($w=$gantt_week_start;$w<=$gantt_week_end;$w++){
	// skip not real weeks
	if(!api_week_check($w)){continue;}
	// skip oldest weeks
	if($w<$gantt_week_minimum){continue;}
	// add week to array
	$gantt_weeks_array[$w]=substr($w,-2);
}
// debug
/*
api_dump($gantt_week_start);
api_dump($gantt_week_end);
api_dump($gantt_weeks_array);
*/
// build table
$table=new strTable(api_text("own_gantt-tr-unvalued"));
$table->addHeader("&nbsp;");
$table->addHeader(api_text("cProjectsTask-property-subject"),"nowrap","100%");
$table->addHeader("&nbsp;");
$table->addHeader(api_text("cProjectsTask-property-planningDays"),"nowrap text-center");
$table->addHeader(api_text("cProjectsTask-property-planningWeek"),"nowrap text-right");
foreach($gantt_weeks_array as $value=>$label){$table->addHeader($label,"text-center",16,($value==date("YW")?"background-color:#D9EDF7":null));}
$table->addHeader("&nbsp;");
// cycle all activities
foreach($activities_array as $activity_fobj){
	// make activity row
	$table->addRow();
	$table->addRowFieldAction(api_url(["scr"=>"activities_view","tab"=>"informations","idActivity"=>$activity_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
	$table->addRowField($activity_fobj->getArea()->getLabelPopup()." - ".$activity_fobj->subject,"nowrap text-strong");
	$table->addRowField("&nbsp;"); // inutile, è sempre attivo $activity_fobj->getStatus()->getLabel(false,true),"nowrap text-right");
	$table->addRowField("&nbsp;");
	$table->addRowField("&nbsp;");
	//$table->addRowField(api_tag("strong",api_date_format($activity_fobj->deadline,api_text("date"))),"nowrap text-right");
	//$table->addRowField(api_link("#","-",null,"btn btn-xs btn-default")." 2021-W01"/*.$activity_fobj->gantt_weekStart()*/." ".api_link("#","+",null,"btn btn-xs btn-default"),"nowrap text-right");
	foreach($gantt_weeks_array as $value=>$label){$table->addRowField("&nbsp;","text-center",($value==date("YW")?"background-color:#D9EDF7":null));}
	// cycle all activity tasks
	foreach($activities_tasks_array[$activity_fobj->id] as $task_fobj){
		// make table row class
		$tr_class_array=array();
		if($task_fobj->id==$_REQUEST["idTask"]){$tr_class_array[]="currentrow";}
		if($task_fobj->deleted){$tr_class_array[]="deleted";}
		// make tasks row
		$table->addRow(implode(" ",$tr_class_array));
		$table->addRowFieldAction(api_url(["scr"=>"activities_view","tab"=>"tasks","act"=>"task_view","idActivity"=>$task_fobj->getActivity()->id,"idTask"=>$task_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
		//$table->addRowField(api_tag("samp",$task_fobj->getActivity()->id.".".$task_fobj->id),"nowrap");
		$table->addRowField("&bull; ".$task_fobj->subject,"nowrap");
		$table->addRowField($task_fobj->getStatus()->getLabel(false,true),"nowrap text-right");
		$table->addRowField($task_fobj->planningDays,"nowrap text-center");
		//$table->addRowField(api_link("#","-",null,"btn btn-xs btn-default")." ".$task_fobj->planningWeek." ".api_link("#","+",null,"btn btn-xs btn-default"),"nowrap text-right");
		$table->addRowField(api_week_format($task_fobj->planningWeek,"W-Y"),"nowrap text-right");
		// cycle all gant weeks
		foreach($gantt_weeks_array as $value=>$label){
			if($value>=$task_fobj->gantt_weekStart() && $value<=$task_fobj->gantt_weekEnd()){$td=api_label("&nbsp;","label-primary");}else{$td="&nbsp;";}
			// check for start and build link
			if($value==$gantt_week_minimum && $task_fobj->gantt_weekStart() && $task_fobj->gantt_weekStart()<$gantt_week_minimum){$td=api_link(api_url(["scr"=>"own_gantt","minimum"=>$task_fobj->gantt_weekStart()]),api_label("<",($task_fobj->gantt_weekEnd()<$gantt_week_minimum?"label-default":"label-primary")));}
			$table->addRowField($td,"text-center",($value==date("YW")?"background-color:#D9EDF7":null));
		}
	}
}
// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($activities_tasks_array,"activities tasks array");
