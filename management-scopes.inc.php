<?php
/**
 * Projects - Management (Scopes)
 *
 * @package Coordinator\Modules\Projects
 * @scope Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build scopes table
$scopes_table=new strTable(api_text("management-scopes-tr-unvalued"));
$scopes_table->addHeader(api_text("cProjectsScope-property-fkArea"),"nowrap");
$scopes_table->addHeader(api_text("cProjectsScope-property-name"),"nowrap");
$scopes_table->addHeader(api_text("cProjectsScope-property-description"),null,"100%");
$scopes_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"scopes","act"=>"scope_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all scopes
foreach(api_sortObjectsArray(cProjectsScope::availables(true),"name") as $scope_fobj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"management","tab"=>"scopes","act"=>"scope_view","idScope"=>$scope_fobj->id]),"fa-info-circle",api_text("table-td-view"));
	$ob->addElement(api_url(["scr"=>"management","tab"=>"scopes","act"=>"scope_edit","idScope"=>$scope_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("projects-manage")));
	if($scope_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsScope","idScope"=>$scope_fobj->id,"return"=>["scr"=>"management","tab"=>"scopes"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cProjectsScope-confirm-undelete"));}
	else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsScope","idScope"=>$scope_fobj->id,"return"=>["scr"=>"management","tab"=>"scopes"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cProjectsScope-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($scope_fobj->id==$_REQUEST["idScope"]){$tr_class_array[]="currentrow";}
	if($scope_fobj->deleted){$tr_class_array[]="deleted";}
	// make projects row
	$scopes_table->addRow(implode(" ",$tr_class_array));
	$scopes_table->addRowField($scope_fobj->getArea()->getLabelPopup(),"nowrap");
	$scopes_table->addRowField($scope_fobj->name,"nowrap");
	$scopes_table->addRowField($scope_fobj->description,"truncate-ellipsis");
	$scopes_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="scope_view"){
	// get selected scope
	$selected_scope_obj=new cProjectsScope($_REQUEST["idScope"]);
	// build description list
	$dl=new strDescriptionList("br","dl-horizontal");
	$dl->addElement(api_text("cProjectsScope-property-fkArea"),$selected_scope_obj->getArea()->getLabelPopup());
	$dl->addElement(api_text("cProjectsScope-property-name"),api_tag("strong",$selected_scope_obj->name));
	if($selected_scope_obj->description){$dl->addElement(api_text("cProjectsScope-property-description"),nl2br($selected_scope_obj->description));}
	// build modal
	$modal=new strModal(api_text("management-scopes-modal-title"),null,"management-scopes-view");
	$modal->setBody($dl->render()."<hr>".api_logs_table($selected_scope_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-scopes-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["scope_add","scope_edit"]) && api_checkAuthorization("projects-manage")){
	// get selected scope
	$selected_scope_obj=new cProjectsScope($_REQUEST["idScope"]);
	// get form
	$form=$selected_scope_obj->form_edit(["return"=>["scr"=>"management","tab"=>"scopes"]]);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_scope_obj->exists()){
		if($selected_scope_obj->isDeleted()){
			$form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsScope","idScope"=>$selected_scope_obj->id,"return"=>["scr"=>"management","tab"=>"scopes"]]),"btn-warning",api_text("cProjectsScope-confirm-undelete"));
			$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cProjectsScope","idScope"=>$selected_scope_obj->id,"return"=>["scr"=>"management","tab"=>"scopes"]]),"btn-danger",api_text("cProjectsScope-confirm-remove"));
		}else{$form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsScope","idScope"=>$selected_scope_obj->id,"return"=>["scr"=>"management","tab"=>"scopes"]]),"btn-danger",api_text("cProjectsScope-confirm-delete"));}
	}
	// build modal
	$modal=new strModal(api_text("management-scopes-modal-title"),null,"management-scopes-edit");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-scopes-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
