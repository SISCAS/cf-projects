<?php
/**
 * Projects - Dashboard
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text(MODULE));
// build dashboard object
$dashboard=new strDashboard();
$dashboard->addTile(api_url(["scr"=>"own_gantt"]),api_text("own_gantt"),api_text("own_gantt-description"),(api_checkAuthorization("projects-usage")),"1x1","fa-tasks");
$dashboard->addTile(api_url(["scr"=>"own_activities"]),api_text("own_activities"),api_text("own_activities-description"),(api_checkAuthorization("projects-usage")),"1x1","fa-th-list");
$dashboard->addTile(api_url(["scr"=>"own_tasks"]),api_text("own_tasks"),api_text("own_tasks-description"),(api_checkAuthorization("projects-usage")),"1x1","fa-list-ul");

$dashboard->addTile(api_url(["scr"=>"activities_gantt"]),api_text("activities_gantt"),api_text("activities_gantt-description"),(api_checkAuthorization("projects-usage")),"1x1","fa-sitemap");
$dashboard->addTile(api_url(["scr"=>"activities_list"]),api_text("activities_list"),api_text("activities_list-description"),(api_checkAuthorization("projects-usage")),"1x1","fa-folder-open-o");
$dashboard->addTile(api_url(["scr"=>"activities_edit"]),api_text("activities_edit-new"),api_text("activities_edit-new-description"),(api_checkAuthorization("projects-usage")),"1x1","fa-file-o");

// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol($dashboard->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
