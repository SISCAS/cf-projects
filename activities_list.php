<?php
/**
 * Projects - Activities List
 *
 * @package Coordinator\Modules\Projects
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("projects-usage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("activities_list"));
// definitions
$activities_array=array();
// build filter
$filter=new strFilter();
$filter->addSearch(["id","subject","description"]);
//$filter->addItem(api_text("cProjectsActivityStatus"),api_transcodingsFromObjects(cProjectsActivityStatus::availables(),"code","text"),"status");  @todo reso dinamico capire come filtrarlo
$filter->addItem(api_text("cProjectsScope"),api_transcodingsFromObjects(cProjectsScope::availables()),"fkScope");
$filter->addItem(api_text("cOrganizationArea"),api_transcodingsFromObjects(cOrganizationArea::availables()),"fkArea");

/** @todo sostituire poi con nuovo cobject user */
$users_array=array();
$results=$GLOBALS['database']->queryObjects("SELECT * FROM `framework__users` ORDER BY lastname,firstname");
foreach($results as $result){$users_array[$result->id]=$result->lastname." ".$result->firstname;}
$filter->addItem(api_text("cProjectsActivity-property-fkUserReferent"),$users_array,"fkUserReferent");

// build query
$query=new cQuery("projects__activities",$filter->getQueryWhere());
$query->addQueryOrderField("id");
// build pagination
$pagination=new strPagination($query->getRecordsCount());
// cycle all results
foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$activities_array[$result_f->id]=new cProjectsActivity($result_f);}
// build table
$table=new strTable(api_text("activities_list-tr-unvalued"));
$table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
$table->addHeader("#","text-center");
$table->addHeader(api_text("cProjectsActivity-property-fkScope"),"nowrap");
//$table->addHeader(api_text("cProjectsActivity-property-fkUserReferent"),"nowrap");
$table->addHeader(api_text("cProjectsActivity-property-fkArea"),"nowrap");
$table->addHeader("&nbsp;");
$table->addHeader(api_text("cProjectsActivity-property-subject"),null,"100%");
$table->addHeader(api_text("cProjectsActivity-property-deadline"),"nowrap text-right");

// @todo check authorization
if(1){$table->addHeaderAction(api_url(["scr"=>"activities_edit"]),"fa-plus",api_text("table-td-add"),null,"text-right");}

// cycle all activities
foreach($activities_array as $activity_fobj){
	// make table row class
	$tr_class_array=array();
	if($activity_fobj->id==$_REQUEST["idActivity"]){$tr_class_array[]="currentrow";}
	if($activity_fobj->deleted){$tr_class_array[]="deleted";}
	// make activities row
	$table->addRow(implode(" ",$tr_class_array));
	$table->addRowFieldAction(api_url(["scr"=>"activities_view","idActivity"=>$activity_fobj->id]),"fa-search",api_text("table-td-view"));
	$table->addRowField(api_tag("samp",$activity_fobj->id),"nowrap");
	$table->addRowField($activity_fobj->getScope()->name,"nowrap");
	//$table->addRowField($activity_fobj->getReferent()->fullname,"nowrap");
	$table->addRowField($activity_fobj->getArea()->getLabelPopup(),"nowrap");
	$table->addRowField($activity_fobj->getStatus()->getLabel(false,true),"nowrap");
	$table->addRowField($activity_fobj->subject,"truncate-ellipsis");
	$table->addRowField(api_date_format($activity_fobj->deadline,api_text("date")),"nowrap text-right");

	// @todo check authorization
	if(1){
		// build operation button
		$ob=new strOperationsButton();
		$ob->addElement(api_url(["scr"=>"activities_edit","idActivity"=>$activity_fobj->id,"return"=>["scr"=>"activities_list"]]),"fa-pencil",api_text("table-td-edit"));
		$ob->addElement(api_url(["scr"=>"activities_view","tab"=>"informations","act"=>"activity_status","idActivity"=>$activity_fobj->id,"return"=>["scr"=>"activities_list"]]),"fa-recycle",api_text("table-td-status"));
		if($activity_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cProjectsActivity","idActivity"=>$activity_fobj->id,"return"=>["scr"=>"activities_list"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cProjectsActivity-confirm-undelete"));}
		else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cProjectsActivity","idActivity"=>$activity_fobj->id,"return"=>["scr"=>"activities_list"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cProjectsActivity-confirm-delete"));}
		// add operation button to table
		$table->addRowField($ob->render(),"nowrap text-right");
	}
}
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($filter->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($pagination->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($query,"query");
